package com.tw;

public class MultiplicationTable {
    public String buildMultiplicationTable(int start, int end) {
        if(!isValid(start, end)) return null;

        return generateTable(start, end);
    }

    public Boolean isValid(int start, int end) {
        return isInRange(start) && isInRange(end) && isStartNotBiggerThanEnd(start, end);
    }

    public Boolean isInRange(int number) {
        return number > 1 && number <= 1000;
    }

    public Boolean isStartNotBiggerThanEnd(int start, int end) {
        return start <= end;
    }

    public String generateTable(int start, int end) {

        String res = "";
        for(int i = start; i <= end; i++){
            res += generateLine(start, i);
            if(i != end) res += '\n';
        }

        return res;
    }

    public String generateLine(int start, int row) {
        String res = "";
        for(int i = start; i <= row; i++){
            if(i != start) res += "  ";
            res += generateSingleExpression(i, row);
        }

        return res;
    }

    public String generateSingleExpression(int multiplicand, int multiplier) {
        return multiplicand + "*" +multiplier + "=" + multiplicand * multiplier;
    }
}
